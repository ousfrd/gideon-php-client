<?php
/**
 * Review
 *
 * PHP version 5
 *
 * @category Class
 * @package  Gideon
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gideon API
 *
 * # Integrating with Gideon.
 *
 * OpenAPI spec version: 0.1.7
 * Contact: neal.wkacc@gmail.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.19
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Gideon\Model;

use \ArrayAccess;
use \Gideon\ObjectSerializer;

/**
 * Review Class Doc Comment
 *
 * @category Class
 * @package  Gideon
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Review implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'review';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
        'asin' => 'string',
        'country' => 'string',
        'reviewDate' => 'string',
        'profileName' => 'string',
        'reviewTitle' => 'string',
        'reviewText' => 'string',
        'reviewRating' => 'int',
        'reviewId' => 'string',
        'reviewLink' => 'string',
        'verifiedPurchase' => 'bool',
        'image' => 'string',
        'video' => 'string',
        'category' => 'string',
        'createdAt' => 'string',
        'updatedAt' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => null,
        'asin' => null,
        'country' => null,
        'reviewDate' => null,
        'profileName' => null,
        'reviewTitle' => null,
        'reviewText' => null,
        'reviewRating' => null,
        'reviewId' => null,
        'reviewLink' => null,
        'verifiedPurchase' => null,
        'image' => null,
        'video' => null,
        'category' => null,
        'createdAt' => null,
        'updatedAt' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'asin' => 'asin',
        'country' => 'country',
        'reviewDate' => 'review_date',
        'profileName' => 'profile_name',
        'reviewTitle' => 'review_title',
        'reviewText' => 'review_text',
        'reviewRating' => 'review_rating',
        'reviewId' => 'review_id',
        'reviewLink' => 'review_link',
        'verifiedPurchase' => 'verified_purchase',
        'image' => 'image',
        'video' => 'video',
        'category' => 'category',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'asin' => 'setAsin',
        'country' => 'setCountry',
        'reviewDate' => 'setReviewDate',
        'profileName' => 'setProfileName',
        'reviewTitle' => 'setReviewTitle',
        'reviewText' => 'setReviewText',
        'reviewRating' => 'setReviewRating',
        'reviewId' => 'setReviewId',
        'reviewLink' => 'setReviewLink',
        'verifiedPurchase' => 'setVerifiedPurchase',
        'image' => 'setImage',
        'video' => 'setVideo',
        'category' => 'setCategory',
        'createdAt' => 'setCreatedAt',
        'updatedAt' => 'setUpdatedAt'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'asin' => 'getAsin',
        'country' => 'getCountry',
        'reviewDate' => 'getReviewDate',
        'profileName' => 'getProfileName',
        'reviewTitle' => 'getReviewTitle',
        'reviewText' => 'getReviewText',
        'reviewRating' => 'getReviewRating',
        'reviewId' => 'getReviewId',
        'reviewLink' => 'getReviewLink',
        'verifiedPurchase' => 'getVerifiedPurchase',
        'image' => 'getImage',
        'video' => 'getVideo',
        'category' => 'getCategory',
        'createdAt' => 'getCreatedAt',
        'updatedAt' => 'getUpdatedAt'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['asin'] = isset($data['asin']) ? $data['asin'] : null;
        $this->container['country'] = isset($data['country']) ? $data['country'] : null;
        $this->container['reviewDate'] = isset($data['reviewDate']) ? $data['reviewDate'] : null;
        $this->container['profileName'] = isset($data['profileName']) ? $data['profileName'] : null;
        $this->container['reviewTitle'] = isset($data['reviewTitle']) ? $data['reviewTitle'] : null;
        $this->container['reviewText'] = isset($data['reviewText']) ? $data['reviewText'] : null;
        $this->container['reviewRating'] = isset($data['reviewRating']) ? $data['reviewRating'] : null;
        $this->container['reviewId'] = isset($data['reviewId']) ? $data['reviewId'] : null;
        $this->container['reviewLink'] = isset($data['reviewLink']) ? $data['reviewLink'] : null;
        $this->container['verifiedPurchase'] = isset($data['verifiedPurchase']) ? $data['verifiedPurchase'] : null;
        $this->container['image'] = isset($data['image']) ? $data['image'] : null;
        $this->container['video'] = isset($data['video']) ? $data['video'] : null;
        $this->container['category'] = isset($data['category']) ? $data['category'] : null;
        $this->container['createdAt'] = isset($data['createdAt']) ? $data['createdAt'] : null;
        $this->container['updatedAt'] = isset($data['updatedAt']) ? $data['updatedAt'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['asin'] === null) {
            $invalidProperties[] = "'asin' can't be null";
        }
        if ($this->container['country'] === null) {
            $invalidProperties[] = "'country' can't be null";
        }
        if ($this->container['reviewDate'] === null) {
            $invalidProperties[] = "'reviewDate' can't be null";
        }
        if ($this->container['profileName'] === null) {
            $invalidProperties[] = "'profileName' can't be null";
        }
        if ($this->container['reviewTitle'] === null) {
            $invalidProperties[] = "'reviewTitle' can't be null";
        }
        if ($this->container['reviewText'] === null) {
            $invalidProperties[] = "'reviewText' can't be null";
        }
        if ($this->container['reviewRating'] === null) {
            $invalidProperties[] = "'reviewRating' can't be null";
        }
        if ($this->container['reviewId'] === null) {
            $invalidProperties[] = "'reviewId' can't be null";
        }
        if ($this->container['reviewLink'] === null) {
            $invalidProperties[] = "'reviewLink' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets asin
     *
     * @return string
     */
    public function getAsin()
    {
        return $this->container['asin'];
    }

    /**
     * Sets asin
     *
     * @param string $asin asin
     *
     * @return $this
     */
    public function setAsin($asin)
    {
        $this->container['asin'] = $asin;

        return $this;
    }

    /**
     * Gets country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->container['country'];
    }

    /**
     * Sets country
     *
     * @param string $country country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->container['country'] = $country;

        return $this;
    }

    /**
     * Gets reviewDate
     *
     * @return string
     */
    public function getReviewDate()
    {
        return $this->container['reviewDate'];
    }

    /**
     * Sets reviewDate
     *
     * @param string $reviewDate reviewDate
     *
     * @return $this
     */
    public function setReviewDate($reviewDate)
    {
        $this->container['reviewDate'] = $reviewDate;

        return $this;
    }

    /**
     * Gets profileName
     *
     * @return string
     */
    public function getProfileName()
    {
        return $this->container['profileName'];
    }

    /**
     * Sets profileName
     *
     * @param string $profileName profileName
     *
     * @return $this
     */
    public function setProfileName($profileName)
    {
        $this->container['profileName'] = $profileName;

        return $this;
    }

    /**
     * Gets reviewTitle
     *
     * @return string
     */
    public function getReviewTitle()
    {
        return $this->container['reviewTitle'];
    }

    /**
     * Sets reviewTitle
     *
     * @param string $reviewTitle reviewTitle
     *
     * @return $this
     */
    public function setReviewTitle($reviewTitle)
    {
        $this->container['reviewTitle'] = $reviewTitle;

        return $this;
    }

    /**
     * Gets reviewText
     *
     * @return string
     */
    public function getReviewText()
    {
        return $this->container['reviewText'];
    }

    /**
     * Sets reviewText
     *
     * @param string $reviewText reviewText
     *
     * @return $this
     */
    public function setReviewText($reviewText)
    {
        $this->container['reviewText'] = $reviewText;

        return $this;
    }

    /**
     * Gets reviewRating
     *
     * @return int
     */
    public function getReviewRating()
    {
        return $this->container['reviewRating'];
    }

    /**
     * Sets reviewRating
     *
     * @param int $reviewRating reviewRating
     *
     * @return $this
     */
    public function setReviewRating($reviewRating)
    {
        $this->container['reviewRating'] = $reviewRating;

        return $this;
    }

    /**
     * Gets reviewId
     *
     * @return string
     */
    public function getReviewId()
    {
        return $this->container['reviewId'];
    }

    /**
     * Sets reviewId
     *
     * @param string $reviewId reviewId
     *
     * @return $this
     */
    public function setReviewId($reviewId)
    {
        $this->container['reviewId'] = $reviewId;

        return $this;
    }

    /**
     * Gets reviewLink
     *
     * @return string
     */
    public function getReviewLink()
    {
        return $this->container['reviewLink'];
    }

    /**
     * Sets reviewLink
     *
     * @param string $reviewLink reviewLink
     *
     * @return $this
     */
    public function setReviewLink($reviewLink)
    {
        $this->container['reviewLink'] = $reviewLink;

        return $this;
    }

    /**
     * Gets verifiedPurchase
     *
     * @return bool
     */
    public function getVerifiedPurchase()
    {
        return $this->container['verifiedPurchase'];
    }

    /**
     * Sets verifiedPurchase
     *
     * @param bool $verifiedPurchase verifiedPurchase
     *
     * @return $this
     */
    public function setVerifiedPurchase($verifiedPurchase)
    {
        $this->container['verifiedPurchase'] = $verifiedPurchase;

        return $this;
    }

    /**
     * Gets image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->container['image'];
    }

    /**
     * Sets image
     *
     * @param string $image image
     *
     * @return $this
     */
    public function setImage($image)
    {
        $this->container['image'] = $image;

        return $this;
    }

    /**
     * Gets video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->container['video'];
    }

    /**
     * Sets video
     *
     * @param string $video video
     *
     * @return $this
     */
    public function setVideo($video)
    {
        $this->container['video'] = $video;

        return $this;
    }

    /**
     * Gets category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     *
     * @param string $category category
     *
     * @return $this
     */
    public function setCategory($category)
    {
        $this->container['category'] = $category;

        return $this;
    }

    /**
     * Gets createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->container['createdAt'];
    }

    /**
     * Sets createdAt
     *
     * @param string $createdAt createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->container['createdAt'] = $createdAt;

        return $this;
    }

    /**
     * Gets updatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->container['updatedAt'];
    }

    /**
     * Sets updatedAt
     *
     * @param string $updatedAt updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->container['updatedAt'] = $updatedAt;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


