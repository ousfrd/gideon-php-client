# Gideon\DisbursesApi

All URIs are relative to *https://300gideon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**disbursePost**](DisbursesApi.md#disbursePost) | **POST** /api/v1/disburses | Create amazon disbursement.
[**disbursesGet**](DisbursesApi.md#disbursesGet) | **GET** /api/v1/disburses | List amazon disbursements


# **disbursePost**
> \Gideon\Model\Disburse disbursePost($disburse)

Create amazon disbursement.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Gideon\Api\DisbursesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$disburse = new \Gideon\Model\Disburse(); // \Gideon\Model\Disburse | 

try {
    $result = $apiInstance->disbursePost($disburse);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisbursesApi->disbursePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **disburse** | [**\Gideon\Model\Disburse**](../Model/Disburse.md)|  |

### Return type

[**\Gideon\Model\Disburse**](../Model/Disburse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **disbursesGet**
> \Gideon\Model\Disburse[] disbursesGet($sellerId, $marketplace, $date)

List amazon disbursements

Lists amazon disbursements.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Gideon\Api\DisbursesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sellerId = "sellerId_example"; // string | Amazon SellerID
$marketplace = "marketplace_example"; // string | Marketplace to check
$date = "date_example"; // string | Disburse date to check

try {
    $result = $apiInstance->disbursesGet($sellerId, $marketplace, $date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisbursesApi->disbursesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sellerId** | **string**| Amazon SellerID | [optional]
 **marketplace** | **string**| Marketplace to check | [optional]
 **date** | **string**| Disburse date to check | [optional]

### Return type

[**\Gideon\Model\Disburse[]**](../Model/Disburse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

