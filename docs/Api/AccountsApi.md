# Gideon\AccountsApi

All URIs are relative to *https://300gideon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsGet**](AccountsApi.md#accountsGet) | **GET** /api/v1/accounts | List amazon seller accounts


# **accountsGet**
> \Gideon\Model\Account[] accountsGet($sellerId, $status)

List amazon seller accounts

Lists amazon seller accounts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Gideon\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sellerId = "sellerId_example"; // string | Amazon SellerID
$status = "status_example"; // string | Amazon seller account status

try {
    $result = $apiInstance->accountsGet($sellerId, $status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->accountsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sellerId** | **string**| Amazon SellerID | [optional]
 **status** | **string**| Amazon seller account status | [optional]

### Return type

[**\Gideon\Model\Account[]**](../Model/Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

