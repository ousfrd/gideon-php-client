# Gideon\ReviewsApi

All URIs are relative to *https://300gideon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**reviewsPost**](ReviewsApi.md#reviewsPost) | **POST** /api/v1/reviews | Create amazon product reviews.


# **reviewsPost**
> \Gideon\Model\Success reviewsPost($reviews)

Create amazon product reviews.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Gideon\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reviews = array(new \Gideon\Model\Review()); // \Gideon\Model\Review[] | 

try {
    $result = $apiInstance->reviewsPost($reviews);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->reviewsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reviews** | [**\Gideon\Model\Review[]**](../Model/Review.md)|  | [optional]

### Return type

[**\Gideon\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

