# Gideon\ImportsApi

All URIs are relative to *https://300gideon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**importsGet**](ImportsApi.md#importsGet) | **GET** /api/v1/imports | List imported amazon reports


# **importsGet**
> \Gideon\Model\Import[] importsGet($sellerId, $marketplace, $reportType, $date)

List imported amazon reports

Lists amazon report imports.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Gideon\Api\ImportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sellerId = "sellerId_example"; // string | Amazon SellerID
$marketplace = "marketplace_example"; // string | Marketplace to check
$reportType = "reportType_example"; // string | Report type to check
$date = "date_example"; // string | Report date to check

try {
    $result = $apiInstance->importsGet($sellerId, $marketplace, $reportType, $date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportsApi->importsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sellerId** | **string**| Amazon SellerID | [optional]
 **marketplace** | **string**| Marketplace to check | [optional]
 **reportType** | **string**| Report type to check | [optional]
 **date** | **string**| Report date to check | [optional]

### Return type

[**\Gideon\Model\Import[]**](../Model/Import.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

