# Disburse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**sellerId** | **string** |  | 
**marketplace** | **string** |  | 
**amount** | **float** |  | 
**totalBalance** | **float** |  | 
**unavailableBalance** | **float** |  | 
**instantTransferBalance** | **float** |  | [optional] 
**requestTransferAvailable** | **bool** |  | 
**disburseDate** | **string** |  | 
**message** | **string** |  | [optional] 
**createdAt** | **string** |  | [optional] 
**updatedAt** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


