# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**asin** | **string** |  | 
**country** | **string** |  | 
**name** | **string** |  | [optional] 
**shortName** | **string** |  | [optional] 
**alias** | **string** |  | [optional] 
**fulfillmentChannel** | **string** |  | [optional] 
**productGroup** | **string** |  | [optional] 
**smallImage** | **string** |  | [optional] 
**mediumImage** | **string** |  | [optional] 
**status** | **int** |  | [optional] 
**discontinued** | **bool** |  | [optional] 
**syncToKbs** | **bool** |  | [optional] 
**length** | **string** |  | [optional] 
**height** | **string** |  | [optional] 
**width** | **string** |  | [optional] 
**weight** | **string** |  | [optional] 
**weighthandling** | **string** |  | [optional] 
**serialNum** | **string** |  | [optional] 
**category** | **string** |  | [optional] 
**expectedMonthlyProfit** | **float** |  | [optional] 
**profitLevel** | **string** |  | [optional] 
**pm** | **string** |  | [optional] 
**listings** | [**\Gideon\Model\Listing[]**](Listing.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


