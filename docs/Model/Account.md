# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**sellerId** | **string** |  | 
**marketplaces** | **string** |  | 
**name** | **string** |  | 
**code** | **string** |  | 
**disburseEnabled** | **bool** |  | [optional] 
**minDisburseAmount** | **float** |  | [optional] 
**status** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


