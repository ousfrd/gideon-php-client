# Import

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**sellerId** | **string** |  | 
**type** | **string** |  | 
**filename** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**reportDate** | **string** |  | [optional] 
**method** | **string** |  | [optional] 
**status** | **string** |  | 
**createdAt** | **string** |  | [optional] 
**updatedAt** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


