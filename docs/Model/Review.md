# Review

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**asin** | **string** |  | 
**country** | **string** |  | 
**reviewDate** | **string** |  | 
**profileName** | **string** |  | 
**reviewTitle** | **string** |  | 
**reviewText** | **string** |  | 
**reviewRating** | **int** |  | 
**reviewId** | **string** |  | 
**reviewLink** | **string** |  | 
**verifiedPurchase** | **bool** |  | [optional] 
**image** | **string** |  | [optional] 
**video** | **string** |  | [optional] 
**category** | **string** |  | [optional] 
**createdAt** | **string** |  | [optional] 
**updatedAt** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


