# Listing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**account** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**asin** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**fnsku** | **string** |  | [optional] 
**hasInnerPage** | **bool** |  | [optional] 
**innerPageAmount** | **float** |  | [optional] 
**amazonQty** | **int** |  | [optional] 
**inboundShippedQuantity** | **int** |  | [optional] 
**amazonTotalQty** | **int** |  | [optional] 
**totalUnits** | **int** |  | [optional] 
**last7DaysUnits** | **int** |  | [optional] 
**last7DaysProfit** | **float** |  | [optional] 
**last30DaysUnits** | **int** |  | [optional] 
**last30DaysProfit** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


